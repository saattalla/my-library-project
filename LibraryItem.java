
public class LibraryItem {

	private String title;
	private int id;
	private int quantity;
	private String format;
	
	public LibraryItem( String title, int id, int quant) {
		this.title = title;
		this.id = id;
		this.quantity = quant;
		
	}
	
	public String getFormat() {
		return format;
	}
	
	public void  setFormat(String f){
		format = f;
	}
	
	public boolean isPeriodical() {
		if (this.format=="periodical") return true;
			else return false;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	

	
}
