import java.util.LinkedList;

public class CD extends LibraryItem {
	private String genre;
	private String artist;
	private String releaseDate;
	private LinkedList<String> awards;
	private LinkedList<String> nominations;
	private int condition;
	
	public CD( String title, int id, int quantity) {
		super(title, id, quantity);
	}
	
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public LinkedList<String> getAwards() {
		return awards;
	}

	public void setAwards(LinkedList<String> awards) {
		this.awards = awards;
	}

	public LinkedList<String> getNominations() {
		return nominations;
	}

	public void setNominations(LinkedList<String> nominations) {
		this.nominations = nominations;
	}

	public int getCondition() {
		return condition;
	}

	public void setCondition(int condition) {
		this.condition = condition;
	}

	
	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
}
