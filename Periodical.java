
public class Periodical extends Book {
	private String genre;
	private String publisher;
	private int volumeNumber;
	
	public Periodical( String title, int id, int quantity) {
		super(title, id, quantity);
		
	}
	
	@Override
	public boolean isPeriodical() {
		return true;
	}
	

	
	@Override 
	public String getGenre() {
		return genre;
	}
	
	@Override
	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getVolumeNumber() {
		return volumeNumber;
	}

	public void setVolumeNumber(int volumeNumber) {
		this.volumeNumber = volumeNumber;
	}
	
	
}

