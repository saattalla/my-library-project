import java.util.LinkedList;

public class Library {
	
	LinkedList<LibraryItem> allItems = new LinkedList<LibraryItem>();

	int branchId;
	
	String branchName;
	
	long phoneNumber;
	
	public Library(int bid, String name, long number) {
		
		this.branchId = bid;
		this.branchName = name;
		this.phoneNumber = number;
		
	}
	
	
	
	public LinkedList<LibraryItem> getAllItems() {
		return allItems;
	}



	public void setAllItems(LinkedList<LibraryItem> allItems) {
		this.allItems = allItems;
	}



	public int getBranchId() {
		return branchId;
	}



	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}



	public String getBranchName() {
		return branchName;
	}



	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}



	public long getPhoneNumber() {
		return phoneNumber;
	}



	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	public void addItem(LibraryItem item) {

		allItems.add(item);
		
	}
	
	public boolean borrowItem(int id) {
		for ( int i=0; i< allItems.size(); i++) {
			
			LibraryItem current= allItems.get(i);
			
			if (current.getId() == id) {
				
				if (current.getQuantity()==0) {
					System.out.println("Sorry, all copies of this item are out at the moment.");
					return false;
				}
				
				else if (current.isPeriodical()) {
					System.out.println("Sorry, you cannot borrow a periodical.");
					return false;
				}
				
				else{
					int remaining = current.getQuantity();
					remaining --;
					current.setQuantity(remaining);
					System.out.println("Item Ok to borrow. Remaining quantity is " + current.getQuantity());
					return true;
				}
				
			}
		}
		
		System.out.println("Sorry we do not carry that item.");
		return false;
		
		
	}

}
