import java.util.LinkedList;

public class Dvd  extends LibraryItem {
	private String genre;
	private String producer;
	private String releaseDate;
	private LinkedList<String> awards;
	private LinkedList<String> nominations;
	private int condition;
	
	
	public Dvd( String title, int id, int quantity) {
		super(title, id, quantity);
	}
	
	
	public String getProducer() {
		return producer;
	}


	public void setProducer(String producer) {
		this.producer = producer;
	}


	public String getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}


	public LinkedList<String> getAwards() {
		return awards;
	}


	public void setAwards(LinkedList<String> awards) {
		this.awards = awards;
	}


	public LinkedList<String> getNominations() {
		return nominations;
	}


	public void setNominations(LinkedList<String> nominations) {
		this.nominations = nominations;
	}


	public int getCondition() {
		return condition;
	}


	public void setCondition(int condition) {
		this.condition = condition;
	}


	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
}
