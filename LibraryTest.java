import static org.junit.Assert.*;

import org.junit.Test;

public class LibraryTest {

	@Test
	public void testSuccessfulBorrow() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		
		mississaugaLibrary.addItem(headFirstJava);
		
		assertTrue(mississaugaLibrary.borrowItem(321));

		
	}

	
	@Test
	public void testQuantityDecrease() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		
		mississaugaLibrary.addItem(headFirstJava);
		mississaugaLibrary.borrowItem(321);

		
		assertEquals(headFirstJava.getQuantity(),1);
	}
	
	
	@Test
	public void testQuantityIncrease() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		LibraryItem todaysPaper = new Periodical ("Toronto Star Aug 25", 221, 1);
		mississaugaLibrary.addItem(headFirstJava);
		mississaugaLibrary.addItem(todaysPaper);
		
		mississaugaLibrary.borrowItem(321);
		headFirstJava.setQuantity(2);
		
		assertEquals(headFirstJava.getQuantity(),2);
	}
	
	@Test
	public void testSuccessfulBorrowMultiple() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		
		mississaugaLibrary.addItem(headFirstJava);
		
		mississaugaLibrary.borrowItem(321);
		assertTrue(mississaugaLibrary.borrowItem(321));
	


		
	}
	
	@Test
	public void testQuantityZero() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		
		mississaugaLibrary.addItem(headFirstJava);
		
		mississaugaLibrary.borrowItem(321);
		mississaugaLibrary.borrowItem(321);

		
		assertEquals(headFirstJava.getQuantity(),0);

		
	}

	
	@Test
	public void testBorrowingQuantityZero() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		
		mississaugaLibrary.addItem(headFirstJava);
		
		mississaugaLibrary.borrowItem(321);
		mississaugaLibrary.borrowItem(321);

		
		assertTrue(!mississaugaLibrary.borrowItem(321));

		
	}
	
	@Test
	public void testBorrowPeriodic() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		LibraryItem todaysPaper = new Periodical ("Toronto Star Aug 25", 221, 1);
		
		mississaugaLibrary.addItem(headFirstJava);
		mississaugaLibrary.addItem(todaysPaper);
		
		assertTrue(!mississaugaLibrary.borrowItem(221));

		
	}
	
	@Test
	public void testBorrowUnavailable() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		LibraryItem todaysPaper = new Periodical ("Toronto Star Aug 25", 221, 1);
		
		mississaugaLibrary.addItem(headFirstJava);
		mississaugaLibrary.addItem(todaysPaper);
		
		assertTrue(!mississaugaLibrary.borrowItem(639));

		
	}
	
	@Test
	public void remainingPeriodicalQuantityUnchanged() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		LibraryItem todaysPaper = new Periodical ("Toronto Star Aug 25", 221, 1);
		
		mississaugaLibrary.addItem(headFirstJava);
		mississaugaLibrary.addItem(todaysPaper);
		mississaugaLibrary.borrowItem(221);
		
		assertEquals(1, todaysPaper.getQuantity());

	}
	
	
	@Test
	public void nonNegativeQuantity() {
		
		Library mississaugaLibrary = new Library(1234, "Mississauga Branch", 4168300664L);
		Book headFirstJava = new Book("HeadFirstJava", 321, 2);
		
		mississaugaLibrary.addItem(headFirstJava);
		mississaugaLibrary.borrowItem(321);
		mississaugaLibrary.borrowItem(321);
		mississaugaLibrary.borrowItem(321);

		
		assertEquals(0, headFirstJava.getQuantity());

	}
	
	
	


}
